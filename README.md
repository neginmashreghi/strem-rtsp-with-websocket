### Description 
Sream rtsp with websocket in react application

---

### WebSockets
WebSockets provide a persistent connection between a client and server that both parties can use to start sending data at any time

--- 

### Stream RTSP with WebSockets


#### Backend

[node-rtsp-stream](https://github.com/kyriesent/node-rtsp-stream), a neat little npm package that allows to convert a RTSP stream into a MPEG-TS stream, over web sockets, that�s compatible with jsmpeg.

`
Stream = require('node-rtsp-stream')
stream = new Stream({
  name: 'name',
  streamUrl: 'rtsp://username:password@172.16.3.18:554',
  wsPort: 9999,
  ffmpegOptions: { // options ffmpeg flags
    '-stats': '', // an option with no neccessary value uses a blank string
    '-r': 30 // options with required values specify the value after the key
  }
})
`

#### Frontend 

[Jsmpeg](https://github.com/phoboslab/jsmpeg) is a Javascript library that allows to visualize such stream into a <canvas> element. 

`
client = new WebSocket('ws://NODEJS_SERVER_IP:9999')
player = new jsmpeg(client, {
  canvas: canvas // Canvas should be a canvas DOM element
})
`

---

### Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

