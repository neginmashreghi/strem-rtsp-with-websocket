import React, { Component } from 'react'
import jsmpeg from 'jsmpeg';
import { w3cwebsocket as W3CWebSocket } from "websocket";
// const client = new W3CWebSocket('ws://localhost:3008');

export default class Canvas extends Component {
  
    componentDidMount() {

        /*  client.onopen = () => {
            console.log('WebSocket Client Connected');
        };
        client.onmessage = (m) => {
        
            const ctx = this.refs.canvas
            const player = new jsmpeg(client, {canvas:ctx});  
            console.log(player)

             if (m.data) {
                if (m.data instanceof Blob) {   
                /// let reader = new FileReader()
                // reader.readAsText(m.data)
                // console.log(reader) 
                /// --------------------
                    var blob = m.data;
                    let canvas = this.refs.canvas
                    let ctx = canvas.getContext("2d");
                    let image = new Image();
                    image.src = blob;
                    image.onload = function() {
                        ctx.drawImage(image, 0, 0);
                    };
                }
            } 
        };  */

      
        const ctx = this.refs.canvas
        const client = new WebSocket( 'ws://localhost:3008' );
        const player = new jsmpeg(client, {canvas:ctx});  
        console.log(player) 
       

     
    }


    render() {


        return (
            <div >
              <canvas ref="canvas" style={{width: "500px", height:"500px"}} /> 
            </div>
        )
    }
}
