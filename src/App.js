import React from 'react';
import Canvas from '../src/Canvas';


function App() {
  return (
    <div className="App">
      <h1>RTSP STREAMING NODE JS IP CAMERA</h1>
      <Canvas/>
    </div>
  );
}

export default App;
